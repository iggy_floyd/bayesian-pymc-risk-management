#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "debian"
__date__ = "$Mar 23, 2015 4:29:56 PM$"


import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np
import matplotlib.pyplot as pl

from cara_utility import * 
from crra_utility import *
from profit_loss import *
import seaborn as  sns

if __name__ == "__main__":  
    sns.set_context("poster")
    
    
    # Create a grid of points for plotting
    gridmax, gridsize = 5000, 1000 # 5000 Euro is the maximal price for a ticket
    grid = np.linspace(0, gridmax, gridsize)


    # 1) CARA
    # Create a new figure
    prefix='utility_'
    pl.figure(figsize=(15,8))
    
    for theta in [-1e-1,3e-2,-3e-3,1e-3, 2e-3,3e-3,4e-3,5e-3,1e-2,2e-2]:
    
        pl.plot(grid, np.array([cara_utility(x,theta,0.02) for x in grid ]), '-', label='CARA: theta = %.3f'%theta)
    
    # Don't forget to label your axes!
    pl.xlabel('$Prise$', fontsize=15)
    pl.ylabel('$u(Prices)$', rotation='horizontal', fontsize=15)
    pl.axis([0, max(grid), -1, 1.1])
    # Add a title to the plot
    pl.title('Utility CARA', fontsize=20, weight='bold')

    # Add a legend
    pl.legend(loc=0, frameon=False)
    label='cara'
    pl.savefig('plots/'+prefix+label+'.png')
    
    pl.show()
    
    
    
    
    # 2) CRRA
    # Create a new figure
    pl.figure(figsize=(15,8))
    
    for theta in [0.50,0.75,1.1,1.5,1.75,2.0,3.0]:
    
        print "theta",theta
        pl.plot(grid, np.array([crra_utility2(x,1,theta,0.01) for x in grid ]), '-', label='CRRA: theta = %.3f'%theta)
    
    # Don't forget to label your axes!
    pl.xlabel('$Prise$', fontsize=15)
    pl.ylabel('$u(Prices)$', rotation='horizontal', fontsize=15)
    
    #pl.axis([0, max(grid), -1, 1.1])
    # Add a title to the plot
    pl.title('Utility CRRA', fontsize=20, weight='bold')

    # Add a legend
    pl.legend(loc=0, frameon=False)    
    label='crra'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    
    # 3) CRRA/CRRA(3000)
    # Create a new figure
    pl.figure(figsize=(15,8))
    
    for theta in [0.50,0.75,1.1,1.5,1.75,2.0,3.0]:
    
        
        pl.plot(grid, np.array([crra_utility2(x,1,theta,0.01)/crra_utility2(3000,1,theta,0.01) for x in grid ]), '-', label='CRRA: theta = %.3f'%theta)
    
    # Don't forget to label your axes!
    pl.xlabel('$Prise$', fontsize=15)
    pl.ylabel('$u(Prices)$', rotation='horizontal', fontsize=15)
    
    #pl.axis([0, max(grid), -1, 1.1])
    # Add a title to the plot
    pl.title('Utility CRRA/CRRRA(3000)', fontsize=20, weight='bold')

    # Add a legend
    pl.legend(loc=0, frameon=False)
    label='crra_crra3000'
    pl.savefig('plots/'+prefix+label+'.png')

    pl.show()
    
    # 4) profit as function of price
    # Create a new figure
    pl.figure(figsize=(15,8))
    for alpha in [0.1,0.2,0.3]:
        pl.plot(grid, np.array([profit_loss_const(x,alpha)[0] for x in grid]), '-', label='profit at const alpha=%.3f'%alpha)
        
    for alpha in [0.1,0.2,0.3]:
        pl.plot(grid, np.array([profit_loss_fromto(x,alpha,4.*alpha,100.,5000.)[0] for x in grid]), '-', 
        label='profit at incresing alpha from %.3f to %.3f for prices from %2.3f to %2.3f'%(alpha,4*alpha,100.,5000.))
    
    
    # Don't forget to label your axes!
    pl.xlabel('$Prise$', fontsize=15)
    pl.ylabel('Profit', rotation='horizontal', fontsize=15)
    
    #pl.axis([0, max(grid), -1, 1.1])
    # Add a title to the plot
    pl.title('Profit', fontsize=20, weight='bold')

    # Add a legend
    pl.legend(loc=0, frameon=False)
    label='profit'
    pl.savefig('plots/'+prefix+label+'.png')

    pl.show()
    
        
    # 5) loss as function of price
    # Create a new figure
    pl.figure(figsize=(15,8))
    for alpha in [0.1,0.2,0.3]:
        pl.plot(grid, np.array([profit_loss_const(x,alpha)[1] for x in grid]), '-', label='loss at const alpha=%.3f'%alpha)
        
    for alpha in [0.1,0.2,0.3]:
        pl.plot(grid, np.array([profit_loss_fromto(x,alpha,4.*alpha,100.,5000.)[1] for x in grid]), '-', 
            label='loss at incresing alpha from %.3f to %.3f for prices from %2.3f to %2.3f '%(alpha,4*alpha,100.,5000.))
    
    
    # Don't forget to label your axes!
    pl.xlabel('$Prise$', fontsize=15)
    pl.ylabel('Loss', rotation='horizontal', fontsize=15)
    
    #pl.axis([0, max(grid), -1, 1.1])
    # Add a title to the plot
    pl.title('Loss', fontsize=20, weight='bold')

    # Add a legend
    pl.legend(loc=0, frameon=False)
    label='loss'
    pl.savefig('plots/'+prefix+label+'.png')
    pl.show()
    