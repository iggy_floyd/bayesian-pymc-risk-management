#! /usr/bin/python

'''
    The module defines utility functions to represent the value of the loos/profit in Euro.
    More details can be found in 
    http://en.wikipedia.org/wiki/Risk_aversion
    
    
'''

__author__ = "debian"
__date__ = "$Mar 23, 2015 2:32:35 PM$"

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9
import numpy as np



def cara_utility(c,theta,_min):
    """Constant absolute risk aversion utility function."""
    utility = 1 - np.exp(-theta * c)
    return min(1.0,max(_min,utility))


if __name__ == "__main__":
    print cara_utility(500,3e-3,1e-2) # loos/profit function @ 500 Euro
